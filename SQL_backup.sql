USE [master]
GO

DECLARE @backupTime VARCHAR(20) =(CONVERT(VARCHAR(8), GETDATE(), 112) +REPLACE(CONVERT(VARCHAR(5), GETDATE(), 114), ':', '')) 
DECLARE @fileName VARCHAR(1000) 
DECLARE @_db NVARCHAR(1000)
DECLARE @i INT = 1
DECLARE @s INT = (SELECT COUNT(*) FROM sys.databases WHERE SUBSTRING(name,1,3) NOT IN ('Tfs') 
AND SUBSTRING(name,1,13) NOT IN ('ReportServer$') 
AND name NOT IN ('master','tempdb','model','msdb'))

WHILE (@i<=@s)
BEGIN

SET @_db =(SELECT name FROM
(SELECT ROW_NUMBER() OVER(ORDER BY name)  AS _row ,name FROM sys.databases 
WHERE SUBSTRING(name,1,3) NOT IN ('Tfs') 
AND SUBSTRING(name,1,13) NOT IN ('ReportServer$') 
AND name NOT IN ('master','tempdb','model','msdb')) a 
WHERE _row=@i)

SELECT
@fileName='D:\DB\backup\'+@_db+'_backup_'+@backupTime+'.bak'
BACKUP DATABASE @_db to disk=@fileName

SET @i=@i+1

END

